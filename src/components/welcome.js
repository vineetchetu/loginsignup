import React, { Component } from 'react';
import Login from './login'

class Welcome extends Component {
    constructor(props) {
        super(props);
    }
    onLogout = () => {
       alert('You have successfully logout.')
       sessionStorage.clear(); // destroy session
    }
    render() {
            return (
                <div>
                    Welcome {sessionStorage.username}
                    <form onSubmit={this.onLogout}>
                         <input type='submit' value='Logout' />
                     </form>
                </div>
             );
    }
}

export default Welcome