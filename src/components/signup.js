import React, { Component } from 'react';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            address: '',
            phone: '',
            age: '',
        };
    }
    mySubmitHandler = (event) => {
        event.preventDefault();
        let fullname = this.state.fullname;
        let address = this.state.address;
        let phone = this.state.phone;
        let age = this.state.age;
        if (fullname === '') {
            alert("Please enter fullname");
        }
        else if (address === '') {
            alert("Please enter address");
        }
        else if (phone === '') {
            alert("Please enter phone");
        }
        else if (age === '') {
            alert("Please enter age");
        } else {
            fetch('https://mywebsite.com/endpoint/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: fullname,
                    add: address,
                    mobile: phone,
                    age: age,
                })
            }).then(
                () => {
                    alert('Success')
                }
            )
        }
    }

    myChangeHandler = (event) => {
        let name = event.target.name;
        let val = event.target.value;
        this.setState({ [name]: val });
    }
    render() {
        return (
            <form onSubmit={this.mySubmitHandler}>
                <table align='center' border='1'>
                    <tr>
                        <td colSpan='2'>
                            <h2>Registration</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Enter your name:</label>
                        </td>
                        <td>
                            <input
                                type='text'
                                name='fullname'
                                onChange={this.myChangeHandler}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Enter your address:</label>
                        </td>
                        <td>
                            <input
                                type='text'
                                name='address'
                                onChange={this.myChangeHandler}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Enter your phone:</label>
                        </td>
                        <td>
                            <input
                                type='text'
                                name='phone'
                                onChange={this.myChangeHandler}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Enter your age:</label>
                        </td>
                        <td>
                            <input
                                type='text'
                                name='age'
                                onChange={this.myChangeHandler}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td colSpan='2'>
                            <input type='submit' />
                        </td>
                    </tr>
                </table>
            </form>
        );
    }
}

export default Signup