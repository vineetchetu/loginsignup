import React, { Component } from 'react';
import Welcome from './welcome'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isLogin: false,
            error: '',
        };
    }

    // default method of React, it automatically run when page render
    componentDidMount() {
        if (sessionStorage.isLogin) {
            this.setState({
                isLogin: true
            })
        }
    }

    mySubmitHandler = (event) => {
        event.preventDefault();
        let user = this.state.username;
        let pass = this.state.password;
        if (user === '') {
            alert("Please enter Username");
        }
        else if (pass === '') {
            alert("Please enter Password");
        }
        else {
            // here call the API to authenticate
            // fetch("https://api.example.com/items")
            //     .then(
            //         (result) => {
            //             this.setState({
            //                 isLogin: true
            //             });
            //             sessionStorage.isLogin = true
            //             sessionStorage.username = this.state.username
            //         },
            //         (error) => {
            //             this.setState({
            //                 error: "There are some error."
            //             });
            //         }
            //     )

            // When we call the API then comment below three lines
            sessionStorage.isLogin = true
            sessionStorage.username = this.state.username
            this.setState({ isLogin: true })
        }

    }

    myChangeHandler = (event) => {
        let name = event.target.name;
        let val = event.target.value;
        this.setState({ [name]: val });
    }
    
    render() {
        if (this.state.isLogin) {
            return (
                <Welcome />
            )
        } else {
            return (
                <form>
                    <table align='center' border='1'>
                        <tr>
                            <td colSpan='2'>
                                <h2>Login</h2>
                                <label style={{color:'red'}}>{this.state.error}</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Enter username:</label>
                            </td>
                            <td>
                                <input
                                    type='text'
                                    name='username'
                                    onChange={this.myChangeHandler}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Enter password:</label>
                            </td>
                            <td>
                                <input
                                    type='password'
                                    name='password'
                                    onChange={this.myChangeHandler}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td colSpan='2'>
                                <input type='button' value='Login' onClick={this.mySubmitHandler} />
                            </td>
                        </tr>
                    </table>
                </form>
            );
        }

    }
}

export default Login